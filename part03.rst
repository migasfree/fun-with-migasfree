.. _`Guía de uso`:

###########
Guía de uso
###########

La reacción al cacahuete.

.. include:: chapter08.rst
.. include:: chapter09.rst
.. include:: chapter10.rst
.. include:: chapter11.rst
.. include:: devices.rst
.. include:: examples.rst
